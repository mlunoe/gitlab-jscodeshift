# gitlab-jscodeshift

This repository is a collection of jscodeshift transformer files to be used to make transformations.

## Install

```sh
yarn install
```

## Run

```sh
$ yarn jscodeshift
```

For instructions on how to use the jscodeshift CLI, please see the [jscodeshift repository](https://github.com/facebook/jscodeshift#usage-cli).

### Example dry-run

#### Check everything in specific directories
```sh
ROOT_DIR=~/workspace/gdk/gitlab; yarn jscodeshift -t=./src/migrateCreateFlash.js --extensions="vue,js" --ignore-pattern="$ROOT_DIR/app/assets/javascripts/locale/*" --ignore-pattern="$ROOT_DIR/spec/frontend/flash_spec.js" "$ROOT_DIR/ee/app/assets/javascripts/" "$ROOT_DIR/ee/spec/frontend/" "$ROOT_DIR/app/assets/javascripts/" "$ROOT_DIR/spec/frontend/" -d
```

#### Check specific files
```sh
ROOT_DIR=~/workspace/gdk/gitlab; echo "spec/frontend/notes/stores/actions_spec.js\nspec/frontend/issues_list/components/issuables_list_app_spec.js\nspec/frontend/notes/components/comment_form_spec.js" | while read line; do echo "$ROOT_DIR/$line"; done | xargs yarn jscodeshift --extensions="vue,js" --ignore-pattern="$ROOT_DIR/app/assets/javascripts/locale/*" --ignore-pattern="$ROOT_DIR/spec/*" --ignore-pattern="$ROOT_DIR/spec/*" -t=./src/migrateCreateFlash.js -d
```

#### Check the files in diff between branch and master
```sh
ROOT_DIR=~/workspace/gdk/gitlab; (cd $ROOT_DIR; git diff --name-only origin/3895-mlunoe-migrate-away-from-deprecated-create-flash origin/master | grep -E "\.(vue|js)$" | while read line; do echo "$ROOT_DIR/$line"; done) | xargs yarn jscodeshift --extensions="vue,js" --ignore-pattern="$ROOT_DIR/app/assets/javascripts/locale/*" --ignore-pattern="$ROOT_DIR/spec/frontend/flash_spec.js" -t=./src/migrateCreateFlash.js -d
```

#### Check the first 1000 files of a specific folder
```sh
FILE_COUNT=1000 ROOT_DIR=~/workspace/gdk/gitlab; (cd $ROOT_DIR; git ls-tree -r HEAD --name-only | grep -E "app/assets/javascripts/.*\.(vue|js)$" --max-count="$FILE_COUNT" | while read line; do echo "$ROOT_DIR/$line"; done) | xargs yarn jscodeshift --extensions="vue,js" -t=./src/migrateCreateFlash.js -d
```

### Example commit message

```md
Refactor(createFlash): use non-deprecated function

Migrate away from `deprecatedCreateFlash` and use
`createFlash` instead.

Codemod used to do this migration:
https://gitlab.com/mlunoe/gitlab-jscodeshift/-/blob/master/src/migrateCreateFlash.js

Epic:
https://gitlab.com/groups/gitlab-org/-/epics/3895
```

### Example Annoucement:

```md
Hello everybody 👋,

This change is about to land migrating the `ee/`-folder away from the `deprecatedCreateFlash` to using `createFlash` 🎉.
https://gitlab.com/gitlab-org/gitlab/-/merge_requests/55549

Because this change is large, it might cause merge conflicts for you when you rebase.
If you run into conflicts during rebase, you can follow this process to reset the file to your original state:
- `git rebase origin/master # start the rebase`
- resolve other conflicts and continue the rebase until you conflicts with `deprecatedCreateFlash` arise
- `git checkout --theirs -- <file_name> # during rebase this will reset file_name to your changes, ignoring changes from origin/master`

If you, instead of rebasing, prefer to merge master into your branch, you can follow this to reset the file to your original state:
- `git merge origin/master # start the merge`
- resolve other conflicts and continue the rebase until you conflicts with `deprecatedCreateFlash` arise
- `git checkout --ours -- <file_name> # during merge this will reset file_name to your changes, ignoring changes from origin/master`

After that is done, you can re-run the codemod on your file like so:
- `cd ~/workspace && git clone git@gitlab.com:mlunoe/gitlab-jscodeshift.git # clone repo into workspace`
- `cd gitlab-jscodeshift`
- `yarn jscodeshift --extensions="vue,js" -t=./src/migrateCreateFlash.js <file_name> # run the codeshift against the file`

If you have any questions, feel free to reach out to me and I'll help you resolve it with you!

Cheers,
Michael
```
