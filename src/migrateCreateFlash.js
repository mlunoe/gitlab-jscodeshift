import adapt from 'vue-jscodeshift-adapter';

const DEPRECATED_CREATE_FLASH_NAME = 'deprecatedCreateFlash';
const NEW_CREATE_FLASH_NAME = 'createFlash';
const indexToArgName = [
  'message',
  'type',
  'parent',
  'actionConfig',
  'fadeTransition',
  'addBodyClass',
];

function updateToCallExps(exps, context) {
  const { file, j, ast } = context;
  exps.replaceWith(nodePath => {
    const { node } = nodePath;

    node.type = 'CallExpression';
    const args = node.arguments;

    if (args.length > indexToArgName.length) {
      throw new Error(`Skipping file: '${file.path}', line: ${node.loc.start.line}. Too many arguments applied!`);
    }
    const newArgs = args.map((arg, index) => {
      const argName = indexToArgName[index];

      return j.property(
        'init',
        j.identifier(argName),
        arg
      );
    });

    node.arguments = [j.objectExpression(newArgs)];
    return node;
  });
}

function updateIdentifiers(identifiers, context) {
  identifiers.replaceWith(nodePath => {
    const { node } = nodePath;
    node.name = NEW_CREATE_FLASH_NAME;
    return node;
  });
}

function updateLocalDefs(localVarNames, context) {
  const { file, j, ast } = context;
  localVarNames.forEach((varName) => {
    const callExps = ast.find(j.CallExpression, { callee: { name: varName } });
    const newExps = ast.find(j.NewExpression, { callee: { name: varName } });
    const specCallExps1 = ast.find(j.CallExpression, { callee: { property: { name: 'toHaveBeenCalledWith' }, object: { callee: { name: 'expect' }, arguments: { 0: { name: varName } } } } });
    const specCallExps2 = ast.find(j.CallExpression, { callee: { property: { name: 'toBeCalledWith' }, object: { callee: { name: 'expect' }, arguments: { 0: { name: varName } } } } });
    const identifiers = ast.find(j.Identifier, { name: varName });

    updateToCallExps(callExps, context);
    updateToCallExps(specCallExps1, context);
    updateToCallExps(specCallExps2, context);
    updateToCallExps(newExps, context);
    updateIdentifiers(identifiers, context);
  });
}

function transformer(file, api) {
  const j = api.jscodeshift;
  const ast = j(file.source);
  const context = { file, j, ast };

  let didChange = false;
  const localVarNames = [];
  const importDefs = ast.find(j.ImportSpecifier, { imported: { name: DEPRECATED_CREATE_FLASH_NAME } });

  importDefs.replaceWith(nodePath => {
    const { node } = nodePath;
    node.type = 'ImportDefaultSpecifier';
    delete node.imported.name;
    // Store for later processing
    localVarNames.push(node.local.name);
    node.local.name = NEW_CREATE_FLASH_NAME;
    didChange = true;
    return node;
  });

  try {
    updateLocalDefs(localVarNames, context);
  } catch (e) {
    console.log(e);
    didChange = false;
  }

  return didChange ? ast.toSource() : null;
}

export default adapt(transformer);
